describe("***** Widget-ECMA5.js test *****", function() {

    var handler1 = function() {
        console.log("Executing handler 1...");
        for (i = 0; i < arguments.length; i++) {
            console.log("Arg[" + i + "] = " + arguments[i]);
        }
    }

    var handler2 = function() {

        doSomething = function() {
            console.log("Executing handler 2...");
            for (i = 0; i < arguments.length; i++) {
                console.log("Arg[" + i + "] = " + arguments[i]);
            }
        }
        doSomething();
    }

    var handler3 = function() {

        doSomething = function() {
            console.log("Executing handler 3...");
            for (i = 0; i < arguments.length; i++) {
                console.log("Arg[" + i + "] = " + arguments[i]);
            }
        }
        doSomething();
    }

    var handler4 = function() {
        doSomething = function() {
            console.log("Executing handler 4...");
            for (var i = 0; i < arguments.length; i++) {
                console.log("Arg[" + i + "] = " + arguments[i]);
            }
        }
        doSomething();
    }

    var x;

    beforeEach(function() {
      x = new OldGenericWidget();
    });


    describe("Validate class implementation", function() {
        it("Class must have defined constructor and methods: add, execute, remove, report", function() {
            expect(x.constructor).toBeDefined();
            expect(x.add).toBeDefined();
            expect(x.execute).toBeDefined();
            expect(x.remove).toBeDefined();
            expect(x.report).toBeDefined();
        });
    });

    describe("Validate subclasses implementation", function() {
        it("'MyTest' which extends 'BaseTest' class inherit methods: add, execute, remove, report", function() {
            var t = new WidgetNumberOne();
            expect(t.constructor).toBeDefined();
            expect(t.add).toBeDefined();
            expect(t.execute).toBeDefined();
            expect(t.remove).toBeDefined();
            expect(t.report).toBeDefined();
        });

        it("'AnotherTest' which extends 'BaseTest' must inherit methods: add, execute, remove, report", function() {
            var t = new WidgetNumberTwo();
            expect(t.constructor).toBeDefined();
            expect(t.add).toBeDefined();
            expect(t.execute).toBeDefined();
            expect(t.remove).toBeDefined();
            expect(t.report).toBeDefined();
        });
    });

    describe("Testing 'add' method:", function() {
        it("Add must add a handler under a given key", function() {
            x.add("key1", handler1);
            expect(x.handlers["key1"]).not.toBeNull();
            expect(x.handlers["key1"][0]).toBe(handler1);
        });

        it("Multiple handlers could be stored under the same key", function() {
            x.add("key1", handler1);
            x.add("key1", handler2);
            x.add("key1", handler3);
            expect(x.handlers["key1"]).not.toBeNull();
            expect(x.handlers["key1"].length).toBe(3);
            expect(x.handlers["key1"][0]).toBe(handler1);
            expect(x.handlers["key1"][1]).toBe(handler2);
            expect(x.handlers["key1"][2]).toBe(handler3);
        });

        it("Handlers could be stored under different keys", function() {
            x.add("key1", handler1);
            x.add("key2", handler2);
            expect(x.handlers["key1"]).not.toBeNull();
            expect(x.handlers["key1"].length).toBe(1);
            expect(x.handlers["key1"][0]).toBe(handler1);
            expect(x.handlers["key2"]).not.toBeNull();
            expect(x.handlers["key2"].length).toBe(1);
            expect(x.handlers["key2"][0]).toBe(handler2);
        });

        it("Same Handler could be stored under different keys", function() {
            x.add("key1", handler1);
            x.add("key2", handler1);
            expect(x.handlers["key1"]).not.toBeNull();
            expect(x.handlers["key1"].length).toBe(1);
            expect(x.handlers["key1"][0]).toBe(handler1);
            expect(x.handlers["key2"]).not.toBeNull();
            expect(x.handlers["key2"].length).toBe(1);
            expect(x.handlers["key2"][0]).toBe(handler1);
        });
    });

    describe("Testing 'remove' method:", function() {

        beforeEach(function() {
            x.add("key1", handler1);
            x.add("key1", handler2);
            x.add("key1", handler3);
            x.add("key1", handler4);
        });

        it("Remove method should be able to remove a handler from first array position", function() {
            expect(x.handlers["key1"].length).toBe(4);
            x.remove("key1", handler1);
            expect(x.handlers["key1"].length).toBe(3);
            expect(x.handlers["key1"][0]).toBe(handler2);
            expect(x.handlers["key1"][1]).toBe(handler3);
            expect(x.handlers["key1"][2]).toBe(handler4);
        });

        it("Remove method should be able to remove a handler from last array position", function() {
            expect(x.handlers["key1"].length).toBe(4);
            x.remove("key1", handler4);
            expect(x.handlers["key1"].length).toBe(3);
            expect(x.handlers["key1"][0]).toBe(handler1);
            expect(x.handlers["key1"][1]).toBe(handler2);
            expect(x.handlers["key1"][2]).toBe(handler3);
        });

        it("Remove method should be able to remove a handler from any array position", function() {
            expect(x.handlers["key1"].length).toBe(4);
            x.remove("key1", handler2);
            expect(x.handlers["key1"].length).toBe(3);
            expect(x.handlers["key1"][0]).toBe(handler1);
            expect(x.handlers["key1"][1]).toBe(handler3);
            expect(x.handlers["key1"][2]).toBe(handler4);
        });

        it("Should be able to remove all handlers under a key", function() {
            expect(x.handlers["key1"].length).toBe(4);
            x.remove("key1", handler4);
            x.remove("key1", handler3);
            x.remove("key1", handler2);
            x.remove("key1", handler1);
            expect(x.handlers["key1"].length).toBe(0);
        });

        it("Try to remove a handler that does not exist under the key should not change array elements", function() {
            var anotherHandler = function() {
                console.log("Just another handler");
            }
            expect(x.handlers["key1"].length).toBe(4);
            x.remove("key1", anotherHandler);
            expect(x.handlers["key1"].length).toBe(4);
        });
    });

    describe("Testing 'execute' method:", function() {
/*
        beforeEach(function() {
            x.add("key1", handler1);
            x.add("key2", handler2);
            x.add("key3", handler3);
            x.add("key4", handler4);
        });
*/
        it ("Execute handler under key = 'key1' but not under keys = 'key2', 'key3', 'key4'", function() {

            var fakeHandler1 = jasmine.createSpy(function(){ console.log("Fake Handler 1"); });
            var fakeHandler2 = jasmine.createSpy(function(){ console.log("Fake Handler 2"); });
            var fakeHandler3 = jasmine.createSpy(function(){ console.log("Fake Handler 3"); });
            var fakeHandler4 = jasmine.createSpy(function(){ console.log("Fake Handler 4"); });

            x.add("key1", fakeHandler1);
            x.add("key2", fakeHandler2);
            x.add("key3", fakeHandler3);
            x.add("key4", fakeHandler4);

            expect(x.handlers["key1"][0]).toBe(fakeHandler1);
            expect(x.handlers["key2"][0]).toBe(fakeHandler2);
            expect(x.handlers["key3"][0]).toBe(fakeHandler3);
            expect(x.handlers["key4"][0]).toBe(fakeHandler4);

            x.execute("key1");
            expect(fakeHandler1).toHaveBeenCalled();
            expect(fakeHandler2).not.toHaveBeenCalled();
            expect(fakeHandler3).not.toHaveBeenCalled();
            expect(fakeHandler4).not.toHaveBeenCalled();
        });

        it ("Execute all handler under key = 'key1'", function() {

            var fakeHandler1 = jasmine.createSpy(function(){ console.log("Fake Handler 1"); });
            var fakeHandler2 = jasmine.createSpy(function(){ console.log("Fake Handler 2"); });
            var fakeHandler3 = jasmine.createSpy(function(){ console.log("Fake Handler 3"); });
            var fakeHandler4 = jasmine.createSpy(function(){ console.log("Fake Handler 4"); });

            x.add("key1", fakeHandler1);
            x.add("key1", fakeHandler2);
            x.add("key1", fakeHandler3);
            x.add("key1", fakeHandler4);

            expect(x.handlers["key1"].length).toBe(4);

            x.execute("key1");
            expect(fakeHandler1).toHaveBeenCalled();
            expect(fakeHandler2).toHaveBeenCalled();
            expect(fakeHandler3).toHaveBeenCalled();
            expect(fakeHandler4).toHaveBeenCalled();
        });

        it ("Execute handler under key = 'key1' with arguments: 100, \"some string\"", function() {

            var fakeHandler1 = jasmine.createSpy(function(){ console.log("Fake Handler 1"); });
            x.add("key1", fakeHandler1);

            expect(x.handlers["key1"].length).toBe(1);

            x.execute("key1", 100, "some string");
            expect(fakeHandler1).toHaveBeenCalledWith(100, "some string");
        });


    });

});
