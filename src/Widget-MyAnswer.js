/*
 * This was more or less the code I wrote during the interview.
 * Not very fancy but does the job.
 */

function GenericWidget() {
    this.handlers = [];
}

/*
 * Stores a handler under a given key.
 *
 * @param String type of handler function.
 * @param Function Handler function.
 */
GenericWidget.prototype.add = function(type, handler) {
    var obj = {};
    obj.type = type;
    obj.handler = handler;
    this.handlers.push(obj);
}

/*
 * Executes all handlers under a given key.
 *
 * @param String type of handler function.
 */
GenericWidget.prototype.execute = function(type) {
    var args = [];
    for (i = 1; i < arguments.length; i++) {
        args.push(arguments[i]);
    }

    this.handlers.forEach(function(obj) {
        if (obj.type === type) {
            obj.handler.apply(this, args);
        }
    });
}

/*
 * Remove Handler.

 * @param String type of handler function.
 * @param Function Handler function.
 */
GenericWidget.prototype.remove = function(type, handler) {
    var newHandlers = [];
    this.handlers.forEach(function(obj) {
        if (obj.type !== type || obj.handler !== handler) {
            newHandlers.push(obj);
        }
    });
    this.handlers = newHandlers;
}

/*
 * Remove handler with a fancy filer.
 * I just adding this method for comparison.
 *
 * @param String type of handler function.
 * @param Function Handler function.
 */
GenericWidget.prototype.removeWithFilter = function(type, handler) {
    var arr = this.handlers;
    this.handlers = arr.filter(function(obj) {
        return obj.type !== type || obj.handler !== handler;
    });
}

GenericWidget.prototype.report = function() {
    console.log(this.handlers);
}

function TestWidget() {
    GenericWidget.call(this);
    //More interesting code for this implementation goes here!!!
}
TestWidget.prototype = Object.create(GenericWidget.prototype);
TestWidget.prototype.constructor = TestWidget;


function AnotherTestWidget() {
    GenericWidget.call(this);
    //More interesting code for this implementation goes here!!!
}
AnotherTestWidget.prototype = Object.create(GenericWidget.prototype);
AnotherTestWidget.prototype.constructor = AnotherTestWidget;
