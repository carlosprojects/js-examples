/*
 * Widget code without Interview pressure in ECMA5 syntax.
 */

function OldGenericWidget() {
    this.handlers = {};
}

/*
 * Stores a handler under a given key.
 *
 * @param String type of handler function.
 * @param Function Handler function.
 */
OldGenericWidget.prototype.add = function(type, handler) {
    if (!this.handlers[type]) {
        this.handlers[type] = [];
    }
    this.handlers[type].push(handler);
}

/*
 * Executes all handlers under a given key.
 *
 * @param String type of handler function.
 */
OldGenericWidget.prototype.execute = function(type) {
    var args = [];
    for (i = 1; i < arguments.length; i++) {
        args.push(arguments[i]);
    }

    if (this.handlers[type]) {
        for(var i = 0; i < this.handlers[type].length; i++) {
            this.handlers[type][i].apply(this, args);
        }
    }
}

/*
 * Removes a handelr under a given key.
 *
 * @param String type of handler function.
 * @param Function Handler function.
 */
OldGenericWidget.prototype.remove = function(type, handler) {
    if (this.handlers[type]) {
        var arr = this.handlers[type];
        this.handlers[type] = arr.filter(function(item) {
            return item !== handler;
        });
    }
}

OldGenericWidget.prototype.report = function() {
    console.log(this.handlers);
}

function WidgetNumberOne() {
    OldGenericWidget.call(this);
    //More interesting code for this implementation goes here!!!
}
WidgetNumberOne.prototype = Object.create(OldGenericWidget.prototype);
WidgetNumberOne.prototype.constructor = WidgetNumberOne;


function WidgetNumberTwo() {
    OldGenericWidget.call(this);
    //More interesting code for this implementation goes here!!!
}
WidgetNumberTwo.prototype = Object.create(OldGenericWidget.prototype);
WidgetNumberTwo.prototype.constructor = WidgetNumberTwo;
