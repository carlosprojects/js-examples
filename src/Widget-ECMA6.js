/*
 * Widget code without Interview pressure in ECMA6 syntax.
 */

class NewGenericWidget {
	constructor() {
		this.handlers = {};
	}

	/*
	 * Stores a handler under a given key.
	 *
	 * @param String type of handler function.
	 * @param Function Handler function.
	 */
	add(type, handler) {
		if (!this.handlers[type]) {
			this.handlers[type] = [];
		}
		this.handlers[type].push(handler);
	}

	/*
	 * Executes all handlers under a given key.
	 *
	 * @param String type of handler function.
	 */
	execute(type) {
		var args = [];
		for (var i = 1; i < arguments.length; i++) {
			args.push(arguments[i]);
		}

		if (this.handlers[type]) {
			this.handlers[type].forEach((h) => {
				h.apply(this, args);
			});
		}
	}

	/*
	 * Removes a handler under a given key.
	 *
	 * @param String type of handler function.
	 * @param Function Handler function.
	 */
	remove(type, handler) {
		if (this.handlers[type]) {
			var arr = this.handlers[type];
			this.handlers[type] = arr.filter(item => item !== handler);
		}
	}

	/**
	 * Just a covenience method.
	 */
	report() {
		console.log(this.handlers);
	}
}

class NewWidgetOne extends NewGenericWidget {
	constructor() {
		super();
		//More interesting code for this implementation goes here!!!
	}
}

class NewWidgetTwo extends NewGenericWidget {
	constructor() {
		super();
		//More interesting code for this implementation goes here!!!
	}
}
