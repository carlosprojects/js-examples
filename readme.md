# JS Examples

The code in this resitory will be a collection of questions resolutions from some interviews

* "src/Widget-MyAnswer.js" is the original code I wrote during the interview.
* "src/Widget-ECMA5.js" and "src/Widget-ECMA6.js" are the same code but with two different level of API. The original question did not require any specific level of API. I just did this for fun and future reference.
* "spec/" contains all Jasmine test cases for the 3 implementations.

## How to run:
* Open "js-test-ecma5.html" or "js-test-ecma6.html" and see your JS console.
* "SpecRunner.js" is stand-alone Jasmines test runner.
